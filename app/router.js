import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
     this.route('project', { path: '/:project_id' }, function() {
        this.route('sketches', { path: '/sketches' } );
     });

     this.route('sketch', { path: '/sketches/:sketch_id' }, function() {
        this.route('graph', { path: '/graph' } );
        this.route('tree', { path: '/tree' } );
    });




});

export default Router;
