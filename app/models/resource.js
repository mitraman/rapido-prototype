import DS from "ember-data";
 
var Resource = DS.Model.extend({
    name: DS.attr('string'),
    description: DS.attr('string'),
    url: DS.attr('string'),
    methods: DS.attr(),
    responses: DS.attr(),
    parent: DS.attr(),
    children: DS.attr(),
    class: DS.attr('string')
});

Resource.reopenClass({
  FIXTURES: [
{ 
    id: '1',
    name: 'A very long name for a resource.',
    description: 'res1', 
    url: '/blah', 
    methods: ['GET','PUT'], 
    responses: [], 
    parent: null, 
    children: ['2','3','4','5','6','7','8'],
    class: DS.attr('string')
},
{ 
    id: '2',
    name: 'res2',
    description: 'res2', 
    url: '/blah2', 
    methods: ['GET','POST'], 
    responses: [], 
    parent: '1', 
    children: [],
    class: DS.attr('string')
},
{ 
    id: '3',
    name: 'res3',
    description: 'res2', 
    url: '/blah2', 
    methods: ['GET','POST'], 
    responses: [], 
    parent: '1', 
    children: [],
    class: DS.attr('string')
},
{ 
    id: '4',
    name: 'res4',
    description: 'res2', 
    url: '/blah2', 
    methods: ['GET','POST'], 
    responses: [], 
    parent: '1', 
    children: [],
    class: DS.attr('string')
},
{ 
    id: '5',
    name: 'res5',
    description: 'res2', 
    url: '/blah2', 
    methods: ['GET','POST'], 
    responses: [], 
    parent: '1', 
    children: [],
    class: DS.attr('string')
},
{ 
    id: '6',
    name: 'res6',
    description: 'res2', 
    url: '/blah2', 
    methods: ['GET','POST'], 
    responses: [], 
    parent: '1', 
    children: [],
    class: DS.attr('string')
},
{ 
    id: '7',
    name: 'res7',
    description: 'res2', 
    url: '/blah2', 
    methods: ['GET','POST'], 
    responses: [], 
    parent: '1', 
    children: [],
    class: DS.attr('string')
},
{ 
    id: '8',
    name: 'res8',
    description: 'res2', 
    url: '/blah2', 
    methods: ['GET','POST'], 
    responses: [], 
    parent: '1', 
    children: [],
    class: DS.attr('string')
},
{ 
    id: '100',
    name: 'parent2',
    description: 'parent 2', 
    url: '/p2', 
    methods: ['GET'], 
    responses: [], 
    parent: null, 
    children: ['101'],
    class: DS.attr('string')
},
{ 
    id: '101',
    name: 'cousin',
    description: 'child of parent 2', 
    url: '/p2', 
    methods: ['GET'], 
    responses: [], 
    parent: '100', 
    children: [],
    class: DS.attr('string')
}

  ]
});
  
export default Resource;
