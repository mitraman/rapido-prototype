import DS from "ember-data";
 
var Sketch = DS.Model.extend({
    name: DS.attr('string'),
    description: DS.attr('string'),
});

Sketch.reopenClass({
  FIXTURES: [
{ 
    id: '14',
    name: 'string',
    description: 'string' 
}
  ]
});

  
export default Sketch;
